<?php

namespace Drupal\datetime_extras\Plugin\Field\FieldFormatter;

use Drupal\Core\Form\FormStateInterface;
use Drupal\datetime\Plugin\Field\FieldFormatter\DateTimeDefaultFormatter;

/**
 * Default formatter for time only field.
 *
 * @FieldFormatter(
 *   id = "time_only_field_default",
 *   label = @Translation("Time only field formatter"),
 *   field_types = {
 *     "time_only_field"
 *   }
 * )
 */
class TimeOnlyFieldFormatter extends DateTimeDefaultFormatter {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'format_type' => 'html_time',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form = parent::settingsForm($form, $form_state);
    $form['format_type']['#access'] = FALSE;
    return $form;
  }

}
