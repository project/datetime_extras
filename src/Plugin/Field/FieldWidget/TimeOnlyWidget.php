<?php

namespace Drupal\datetime_extras\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\datetime\Plugin\Field\FieldWidget\DateTimeDefaultWidget;

/**
 * Default widget for time only field.
 *
 * @FieldWidget(
 *   id = "time_only_field_default",
 *   label = @Translation("Time only widget"),
 *   field_types = {
 *     "time_only_field"
 *   }
 * )
 */
class TimeOnlyWidget extends DateTimeDefaultWidget {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);

    $date_type = 'none';
    $time_type = 'time';
    $date_format = $this->dateStorage->load('html_date')->getPattern();
    $time_format = '';

    $element['value'] = array_merge($element['value'], [
      '#date_date_format' => $date_format,
      '#date_date_element' => $date_type,
      '#date_date_callbacks' => [],
      '#date_time_format' => $time_format,
      '#date_time_element' => $time_type,
      '#date_time_callbacks' => [],
    ]);

    return $element;
  }

}
