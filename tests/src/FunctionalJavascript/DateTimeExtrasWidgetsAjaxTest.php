<?php

namespace Drupal\Tests\datetime_extras\FunctionalJavascript;

use Drupal\FunctionalJavascriptTests\WebDriverTestBase;

/**
 * Tests the datetime_extras widgets with AJAX.
 *
 * @group datetime_extras
 */
class DateTimeExtrasWidgetsAjaxTest extends WebDriverTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['datetime_extras_test'];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->drupalLogin($this->drupalCreateUser(['create datetime_extras_test content']));
  }

  /**
   * Tests different date part orders configurations in ajax rebuilt form.
   *
   * @dataProvider testDatePartOrderAjaxRebuildDataProvider
   */
  public function testDatePartOrderAjaxRebuild($config, array $date_part_order) {
    // Apply date part order configuration.
    $display = \Drupal::configFactory()
      ->getEditable('core.entity_form_display.node.datetime_extras_test.default');
    $display
      ->set('content.field_test_date.settings.date_order', $config)->save();

    $this->drupalGet('node/add/datetime_extras_test');
    $page = $this->getSession()->getPage();

    // Check field widget is present and has correct date part order.
    $this->assertSession()->elementExists('css', '.field--name-field-test-date');
    $wrapper = $page->find('css', '.field--name-field-test-date');
    foreach ($date_part_order as $index => $date_part) {
      $index = $index + 1;
      $this->assertSession()->elementExists('css', ".form-item-field-test-date-0-value-{$date_part}:nth-child($index)", $wrapper);
    }

    // Trigger ajax rebuild.
    $page->pressButton('Rebuild');
    $this->assertSession()->assertWaitOnAjaxRequest();

    // Redo the check to make sure ajax form rebuild have not influenced order.
    $this->assertSession()->elementExists('css', '.field--name-field-test-date');
    $wrapper = $page->find('css', '.field--name-field-test-date');
    foreach ($date_part_order as $index => $date_part) {
      $index = $index + 1;
      $this->assertSession()->elementExists('css', ".form-item-field-test-date-0-value-{$date_part}:nth-child($index)", $wrapper);
    }
  }

  /**
   * Data provider for testDatePartOrderAjaxRebuild().
   */
  public function testDatePartOrderAjaxRebuildDataProvider() {
    return [
      ['MDY', ['month', 'day', 'year', 'hour', 'minute']],
      ['DMY', ['day', 'month', 'year', 'hour', 'minute']],
      ['YMD', ['year', 'month', 'day', 'hour', 'minute']],
      ['Y', ['year']],
      ['MY', ['month', 'year']],
      ['YM', ['year', 'month']],
    ];
  }

}
