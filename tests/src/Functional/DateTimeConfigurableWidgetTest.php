<?php

namespace Drupal\Tests\datetime_extras\Functional;

use Drupal\Component\Utility\Unicode;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Datetime\Entity\DateFormat;
use Drupal\Core\Entity\Entity\EntityFormDisplay;
use Drupal\Core\Entity\Entity\EntityViewDisplay;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItem;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\Tests\datetime\Functional\DateTestBase;

/**
 * Tests the configurable date and time widget.
 *
 * @group datetime_extras
 */
class DateTimeConfigurableWidgetTest extends DateTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['datetime_extras', 'node', 'entity_test', 'datetime', 'field_ui'];

  /**
   * {@inheritdoc}
   */
  protected function getTestFieldType() {
    return 'datetime';
  }

  /**
   * {@inheritdoc}
   */
  protected function createField() {
    $field_name = mb_strtolower($this->randomMachineName());
    $field_label = Unicode::ucfirst(mb_strtolower($this->randomMachineName()));
    $type = $this->getTestFieldType();
    $formatter_type = $type . '_default';

    $this->fieldStorage = FieldStorageConfig::create([
      'field_name' => $field_name,
      'entity_type' => 'entity_test',
      'type' => $type,
      'settings' => ['datetime_type' => DateTimeItem::DATETIME_TYPE_DATETIME],
    ]);
    $this->fieldStorage->save();
    $this->field = FieldConfig::create([
      'field_storage' => $this->fieldStorage,
      'label' => $field_label,
      'bundle' => 'entity_test',
      'description' => 'Description for ' . $field_label,
      'required' => TRUE,
    ]);
    $this->field->save();

    EntityFormDisplay::load('entity_test.entity_test.default')
      ->setComponent($field_name, [
        'type' => 'datatime_configurable',
        'settings' => [
          'year_range' => '1900:1950',
          'increment' => '5',
        ],
      ])
      ->save();

    $this->displayOptions = [
      'type' => $formatter_type,
      'label' => 'above',
    ];
    EntityViewDisplay::create([
      'targetEntityType' => $this->field->getTargetEntityTypeId(),
      'bundle' => $this->field->getTargetBundle(),
      'mode' => 'full',
      'status' => TRUE,
    ])->setComponent($field_name, $this->displayOptions)->save();
  }

  /**
   * Tests the configurable date and time widget.
   */
  public function testConfigurableWidget() {
    $field_name = $this->fieldStorage->getName();
    $field_label = $this->field->label();

    // Display creation form and check for the field.
    $this->drupalGet('entity_test/add');
    $this->assertSession()->fieldValueEquals("{$field_name}[0][value][date]", '');
    $this->assertSession()->fieldValueEquals("{$field_name}[0][value][time]", '');
    $this->assertSession()->elementTextContains('xpath', '//fieldset[@id="edit-' . $field_name . '-0"]/legend', $field_label);
    $this->assertSession()->elementExists('xpath', '//fieldset[@aria-describedby="edit-' . $field_name . '-0--description"]');
    $this->assertSession()->elementExists('xpath', '//div[@id="edit-' . $field_name . '-0--description"]');

    // Make sure configured attributes are present.
    $this->assertSession()->elementAttributeContains('xpath', '//input[@id="edit-' . $field_name . '-0-value-date"]', 'min', '1900-01-01');
    $this->assertSession()->elementAttributeContains('xpath', '//input[@id="edit-' . $field_name . '-0-value-date"]', 'max', '1950-12-31');
    $this->assertSession()->elementAttributeContains('xpath', '//input[@id="edit-' . $field_name . '-0-value-time"]', 'step', '5');

    // Build up a valid date in the UTC timezone.
    $date = new DrupalDateTime('1950-01-01 00:00:15', 'UTC');
    $date->setTimezone(timezone_open(date_default_timezone_get()));

    // Submit a value and check if saved.
    $date_format = DateFormat::load('html_date')->getPattern();
    $time_format = DateFormat::load('html_time')->getPattern();
    $this->submitForm([
      "{$field_name}[0][value][date]" => $date->format($date_format),
      "{$field_name}[0][value][time]" => $date->format($time_format),
    ], 'Save');
    preg_match('|entity_test/manage/(\d)|', $this->getUrl(), $match);
    $id = $match[1];
    $this->assertSession()->pageTextContains('entity_test ' . $id . ' has been created.');
  }

}
