<?php

namespace Drupal\Tests\datetime_extras\Functional;

use Drupal\Component\Utility\Unicode;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Datetime\Entity\DateFormat;
use Drupal\Core\Entity\Entity\EntityFormDisplay;
use Drupal\Core\Entity\Entity\EntityViewDisplay;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\Tests\datetime\Functional\DateTestBase;

/**
 * Tests the time only field.
 *
 * @group datetime_extras
 */
class TimeOnlyFieldTest extends DateTestBase {

  /**
   * The default display settings to use for the formatters.
   *
   * @var array
   */
  protected $defaultSettings = ['timezone_override' => ''];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['datetime_extras', 'node', 'entity_test', 'datetime', 'field_ui'];

  /**
   * {@inheritdoc}
   */
  protected function getTestFieldType() {
    return 'time_only_field';
  }

  /**
   * {@inheritdoc}
   */
  protected function createField() {
    $field_name = mb_strtolower($this->randomMachineName());
    $field_label = Unicode::ucfirst(mb_strtolower($this->randomMachineName()));
    $type = $this->getTestFieldType();
    $widget_type = $formatter_type = $type . '_default';

    $this->fieldStorage = FieldStorageConfig::create([
      'field_name' => $field_name,
      'entity_type' => 'entity_test',
      'type' => $type,
      'settings' => [],
    ]);
    $this->fieldStorage->save();
    $this->field = FieldConfig::create([
      'field_storage' => $this->fieldStorage,
      'label' => $field_label,
      'bundle' => 'entity_test',
      'description' => 'Description for ' . $field_label,
      'required' => TRUE,
    ]);
    $this->field->save();

    EntityFormDisplay::load('entity_test.entity_test.default')
      ->setComponent($field_name, ['type' => $widget_type])
      ->save();

    $this->displayOptions = [
      'type' => $formatter_type,
      'label' => 'above',
    ];
    EntityViewDisplay::create([
      'targetEntityType' => $this->field->getTargetEntityTypeId(),
      'bundle' => $this->field->getTargetBundle(),
      'mode' => 'full',
      'status' => TRUE,
    ])->setComponent($field_name, $this->displayOptions)
      ->save();
  }

  /**
   * Tests the time only field, creat.
   */
  public function testTimeonlyField() {
    $field_name = $this->fieldStorage->getName();
    $field_label = $this->field->label();

    // Display creation form.
    $this->drupalGet('entity_test/add');
    $this->assertSession()->fieldNotExists("{$field_name}[0][value][date]");
    $this->assertSession()->fieldValueEquals("{$field_name}[0][value][time]", '');
    $this->assertSession()->elementTextContains('xpath', '//fieldset[@id="edit-' . $field_name . '-0"]/legend', $field_label);
    $this->assertSession()->elementExists('xpath', '//fieldset[@aria-describedby="edit-' . $field_name . '-0--description"]');
    $this->assertSession()->elementExists('xpath', '//div[@id="edit-' . $field_name . '-0--description"]');

    // Build up a date in the UTC timezone.
    $value = '2024-01-26 16:20:00';
    $date = new DrupalDateTime($value, 'UTC');

    // Update the timezone to the system default.
    $date->setTimezone(timezone_open(date_default_timezone_get()));

    // Submit a valid time and ensure it is accepted.
    $time_format = DateFormat::load('html_time')->getPattern();
    $this->submitForm([
      "{$field_name}[0][value][time]" => $date->format($time_format),
    ], 'Save');
    preg_match('|entity_test/manage/(\d+)|', $this->getUrl(), $match);
    $id = $match[1];
    $this->assertSession()->pageTextContains('entity_test ' . $id . ' has been created.');

    // Go to entity page and ensure the time is displayed correctly.
    $this->drupalGet('entity_test/' . $id);
    $this->assertSession()->pageTextContains($field_label);
    $this->assertSession()->pageTextContains($date->format($time_format));
  }

}
