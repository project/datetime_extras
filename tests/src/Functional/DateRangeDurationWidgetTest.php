<?php

namespace Drupal\Tests\datetime_extras\Functional;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Datetime\Entity\DateFormat;
use Drupal\Core\Entity\Display\EntityFormDisplayInterface;
use Drupal\datetime_range\Plugin\Field\FieldType\DateRangeItem;
use Drupal\Tests\datetime\Functional\DateTestBase;

/**
 * Test the DateRangeDurationWidget for datetime_range fields.
 *
 * @group datetime_extras
 */
class DateRangeDurationWidgetTest extends DateTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'datetime_range',
    'datetime_extras',
    'duration_field',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function getTestFieldType(): string {
    return 'daterange';
  }

  /**
   * The default display settings to use for the formatters.
   *
   * @var array
   */
  protected $defaultSettings = ['timezone_override' => '', 'separator' => '-'];

  /**
   * Gets an entity form display.
   *
   * @param string $entity_type_id
   *   The entity type ID.
   * @param string $bundle_id
   *   The entity bundle ID.
   * @param string $form_mode
   *   Which form mode to load.
   *
   * @return \Drupal\Core\Entity\Display\EntityFormDisplayInterface
   *   The entity form display associated with the given form mode.
   */
  protected function getFormDisplay(string $entity_type_id, string $bundle_id, string $form_mode): EntityFormDisplayInterface {
    return \Drupal::service('entity_display.repository')->getFormDisplay($entity_type_id, $bundle_id, $form_mode);
  }

  /**
   * Tests Date Range Duration Widget functionality for datetime fields.
   */
  public function testDateRangeDurationWidget(): void {
    $field_name = $this->fieldStorage->getName();
    $field_label = $this->field->label();

    $session = $this->getSession();
    $page = $session->getPage();

    // Ensure field is set to a datetime field.
    $this->fieldStorage->setSetting('datetime_type', DateRangeItem::DATETIME_TYPE_DATETIME);
    $this->fieldStorage->save();

    // Change the widget to a daterange_duration with some default settings.
    $this->getFormDisplay($this->field->getTargetEntityTypeId(), $this->field->getTargetBundle(), 'default')
      ->setComponent($field_name, [
        'type' => 'daterange_duration',
        'settings' => [
          'duration_granularity' => 'd:h:i',
          'time_increment' => '300',
          'default_duration' => [
            'h' => '2',
            'i' => '15',
          ],
        ],
      ])
      ->save();
    $this->container->get('entity_field.manager')->clearCachedFieldDefinitions();

    // Display creation form.
    $this->drupalGet('entity_test/add');
    $this->assertSession()->fieldValueEquals("{$field_name}[0][value][date]", '');
    $this->assertSession()->fieldValueEquals("{$field_name}[0][value][time]", '');

    $end_type_id = "edit-{$field_name}-0-end-type";
    $duration_radio = $this->xpath('//div[@id=:id]//input[@value=:value]', [':id' => $end_type_id, ':value' => 'duration']);
    $this->assertCount(1, $duration_radio, 'A radio button has a "Duration" choice.');
    $end_date_radio = $this->xpath('//div[@id=:id]//input[@value=:value]', [':id' => $end_type_id, ':value' => 'end_date']);
    $this->assertCount(1, $end_date_radio, 'A radio button has an "End date" choice.');

    // No JS, these should still be visible.
    $this->assertSession()->fieldValueEquals("{$field_name}[0][end_value][date]", '');
    $this->assertSession()->fieldValueEquals("{$field_name}[0][end_value][time]", '');

    // Check the duration field elements.
    // Make sure granularity setting works so that  y, m and s are gone:
    $this->assertEmpty($this->xpath("//input[@id = 'edit-{$field_name}-0-duration-y']"), 'Duration years is not shown.');
    $this->assertEmpty($this->xpath("//input[@id = 'edit-{$field_name}-0-duration-m']"), 'Duration months is not shown.');
    $this->assertEmpty($this->xpath("//input[@id = 'edit-{$field_name}-0-duration-s']"), 'Duration seconds is not shown.');

    // Make sure the default duration setting works on the remaining elements:
    $this->assertEquals('0', current($this->xpath("//input[@id = 'edit-{$field_name}-0-duration-d']"))->getAttribute('value'), 'Default duration days is set correctly.');
    $this->assertEquals('2', current($this->xpath("//input[@id = 'edit-{$field_name}-0-duration-h']"))->getAttribute('value'), 'Default duration hours is set correctly.');
    $this->assertEquals('15', current($this->xpath("//input[@id = 'edit-{$field_name}-0-duration-i']"))->getAttribute('value'), 'Default duration minutes is set correctly.');

    // Build up dates in the UTC timezone.
    $value = '1917-11-07 00:00:00';
    $start_date = new DrupalDateTime($value, 'UTC');
    $date_format = DateFormat::load('html_date')->getPattern();
    $time_format = DateFormat::load('html_time')->getPattern();

    // First, submit a start date, using the default duration (and no end
    // date) and ensure the end_value is set correctly:
    $edit = [
      "{$field_name}[0][value][date]" => $start_date->format($date_format),
      "{$field_name}[0][value][time]" => $start_date->format($time_format),
      "{$field_name}[0][end_type]" => 'duration',
    ];
    $this->submitForm($edit, t('Save'));
    preg_match('|entity_test/manage/(\d+)|', $this->getUrl(), $match);
    $id = $match[1];
    $this->assertSession()->pageTextContains(t('entity_test @id has been created.', ['@id' => $id]));

    $this->assertSession()->responseContains($start_date->format($date_format));
    $this->assertSession()->responseContains($start_date->format($time_format));
    // The end date is hidden by default, so we can't just assertRaw() for the
    // date or time. Instead, assert the field values in the widget.
    $this->assertEquals('0', current($this->xpath("//input[@id = 'edit-{$field_name}-0-duration-d']"))->getAttribute('value'), 'Duration days is set correctly.');
    $this->assertEquals('2', current($this->xpath("//input[@id = 'edit-{$field_name}-0-duration-h']"))->getAttribute('value'), 'Duration hours is set correctly.');
    $this->assertEquals('15', current($this->xpath("//input[@id = 'edit-{$field_name}-0-duration-i']"))->getAttribute('value'), 'Duration minutes is set correctly.');
    $this->assertEquals('1917-11-07', current($this->xpath("//input[@id = 'edit-{$field_name}-0-end-value-date']"))->getAttribute('value'), 'End date is set correctly.');
    $this->assertEquals('02:15:00', current($this->xpath("//input[@id = 'edit-{$field_name}-0-end-value-time']"))->getAttribute('value'), 'End time is set correctly.');

    // Now, submit a start date, change the duration, still no end date:
    $start_date = new DrupalDateTime('1917-11-07 03:05:00', 'UTC');
    $edit = [
      "{$field_name}[0][value][date]" => $start_date->format($date_format),
      "{$field_name}[0][value][time]" => $start_date->format($time_format),
      "{$field_name}[0][end_type]" => 'duration',
      // 10 days that shook the world.
      "{$field_name}[0][duration][d]" => 10,
      // And a little more, to keep things interesting.
      "{$field_name}[0][duration][h]" => 1,
      "{$field_name}[0][duration][i]" => 30,
    ];
    $this->submitForm($edit, t('Save'));

    // Make sure the new start time is happening.
    $this->assertSession()->responseContains($start_date->format($date_format));
    $this->assertSession()->responseContains($start_date->format($time_format));

    // Check that the duration and end_value is correct:
    $this->assertEquals('10', current($this->xpath("//input[@id = 'edit-{$field_name}-0-duration-d']"))->getAttribute('value'), 'Duration days is set correctly.');
    $this->assertEquals('1', current($this->xpath("//input[@id = 'edit-{$field_name}-0-duration-h']"))->getAttribute('value'), 'Duration hours is set correctly.');
    $this->assertEquals('30', current($this->xpath("//input[@id = 'edit-{$field_name}-0-duration-i']"))->getAttribute('value'), 'Duration minutes is set correctly.');
    $this->assertEquals('1917-11-17', current($this->xpath("//input[@id = 'edit-{$field_name}-0-end-value-date']"))->getAttribute('value'), 'End date is set correctly.');
    $this->assertEquals('04:35:00', current($this->xpath("//input[@id = 'edit-{$field_name}-0-end-value-time']"))->getAttribute('value'), 'End time is set correctly.');

    // Now, set a new end time directly, leaving the duration values in the
    // form, and see that when the page reloads, we have the right end time and
    // the correct new duration values pre-filled.
    $edit = [
      "{$field_name}[0][value][date]" => $start_date->format($date_format),
      "{$field_name}[0][value][time]" => $start_date->format($time_format),
      "{$field_name}[0][end_type]" => 'end_date',
      "{$field_name}[0][end_value][date]" => '1917-11-11',
      "{$field_name}[0][end_value][time]" => '08:45:00',
    ];

    $this->submitForm($edit, t('Save'));

    $this->assertSession()->responseContains($start_date->format($date_format));
    $this->assertSession()->responseContains($start_date->format($time_format));

    // start_date is '1917-11-07 03:05:00'.
    // end_value is  '1917-11-11 08:45:00'.
    // Duration should be 4 days, 5 hours, 40 minutes.
    $this->assertEquals('4', current($this->xpath("//input[@id = 'edit-{$field_name}-0-duration-d']"))->getAttribute('value'), 'Duration days is set correctly.');
    $this->assertEquals('5', current($this->xpath("//input[@id = 'edit-{$field_name}-0-duration-h']"))->getAttribute('value'), 'Duration hours is set correctly.');
    $this->assertEquals('40', current($this->xpath("//input[@id = 'edit-{$field_name}-0-duration-i']"))->getAttribute('value'), 'Duration minutes is set correctly.');
    $this->assertEquals('1917-11-11', current($this->xpath("//input[@id = 'edit-{$field_name}-0-end-value-date']"))->getAttribute('value'), 'End date is set correctly.');
    $this->assertEquals('08:45:00', current($this->xpath("//input[@id = 'edit-{$field_name}-0-end-value-time']"))->getAttribute('value'), 'End time is set correctly.');

    // Now, change the widget settings to use the full duration granularity.
    // Change the widget to a daterange_duration with some default settings.
    $this->getFormDisplay($this->field->getTargetEntityTypeId(), $this->field->getTargetBundle(), 'default')
      ->setComponent($field_name, [
        'type' => 'daterange_duration',
        'settings' => [
          'duration_granularity' => 'y:m:d:h:i:s',
          'time_increment' => '1',
          'default_duration' => [
            'd' => '2',
            'h' => '4',
            'i' => '20',
            's' => '5',
          ],
        ],
      ])
      ->save();
    $this->container->get('entity_field.manager')->clearCachedFieldDefinitions();

    // Start over with a new entity.
    $this->drupalGet('entity_test/add');

    // Make sure granularity works and we see all the duration elements:
    $this->assertEquals('0', current($this->xpath("//input[@id = 'edit-{$field_name}-0-duration-y']"))->getAttribute('value'), 'Default duration years is set correctly.');
    $this->assertEquals('0', current($this->xpath("//input[@id = 'edit-{$field_name}-0-duration-m']"))->getAttribute('value'), 'Default duration months is set correctly.');
    $this->assertEquals('2', current($this->xpath("//input[@id = 'edit-{$field_name}-0-duration-d']"))->getAttribute('value'), 'Default duration days is set correctly.');
    $this->assertEquals('4', current($this->xpath("//input[@id = 'edit-{$field_name}-0-duration-h']"))->getAttribute('value'), 'Default duration hours is set correctly.');
    $this->assertEquals('20', current($this->xpath("//input[@id = 'edit-{$field_name}-0-duration-i']"))->getAttribute('value'), 'Default duration minutes is set correctly.');
    $this->assertEquals('5', current($this->xpath("//input[@id = 'edit-{$field_name}-0-duration-s']"))->getAttribute('value'), 'Default duration seconds is set correctly.');

    // Ensure the default duration works with all these elements.
    $edit = [
      "{$field_name}[0][value][date]" => $start_date->format($date_format),
      "{$field_name}[0][value][time]" => $start_date->format($time_format),
      "{$field_name}[0][end_type]" => 'duration',
    ];
    $this->submitForm($edit, t('Save'));
    preg_match('|entity_test/manage/(\d+)|', $this->getUrl(), $match);
    $id = $match[1];
    $this->assertSession()->pageTextContains(t('entity_test @id has been created.', ['@id' => $id]));

    $this->assertSession()->responseContains($start_date->format($date_format));
    $this->assertSession()->responseContains($start_date->format($time_format));

    // None of this should have changed.
    $this->assertEquals('0', current($this->xpath("//input[@id = 'edit-{$field_name}-0-duration-y']"))->getAttribute('value'), 'Duration years is set correctly.');
    $this->assertEquals('0', current($this->xpath("//input[@id = 'edit-{$field_name}-0-duration-m']"))->getAttribute('value'), 'Duration months is set correctly.');
    $this->assertEquals('2', current($this->xpath("//input[@id = 'edit-{$field_name}-0-duration-d']"))->getAttribute('value'), 'Duration days is set correctly.');
    $this->assertEquals('4', current($this->xpath("//input[@id = 'edit-{$field_name}-0-duration-h']"))->getAttribute('value'), 'Duration hours is set correctly.');
    $this->assertEquals('20', current($this->xpath("//input[@id = 'edit-{$field_name}-0-duration-i']"))->getAttribute('value'), 'Duration minutes is set correctly.');
    $this->assertEquals('5', current($this->xpath("//input[@id = 'edit-{$field_name}-0-duration-s']"))->getAttribute('value'), 'Duration seconds is set correctly.');

    // start_date is '1917-11-07 03:05:00'.
    // end_value should be '1917-11-09 07:25:05'.
    $this->assertEquals('1917-11-09', current($this->xpath("//input[@id = 'edit-{$field_name}-0-end-value-date']"))->getAttribute('value'), 'End date is set correctly.');
    $this->assertEquals('07:25:05', current($this->xpath("//input[@id = 'edit-{$field_name}-0-end-value-time']"))->getAttribute('value'), 'End time is set correctly.');

    // Try to use all the duration elements with unique values.
    $edit = [
      "{$field_name}[0][value][date]" => $start_date->format($date_format),
      "{$field_name}[0][value][time]" => $start_date->format($time_format),
      "{$field_name}[0][end_type]" => 'duration',
      "{$field_name}[0][duration][y]" => 2,
      "{$field_name}[0][duration][m]" => 1,
      "{$field_name}[0][duration][d]" => 4,
      "{$field_name}[0][duration][h]" => 5,
      "{$field_name}[0][duration][i]" => 10,
      "{$field_name}[0][duration][s]" => 45,
    ];
    $this->submitForm($edit, t('Save'));

    $this->assertSession()->responseContains($start_date->format($date_format));
    $this->assertSession()->responseContains($start_date->format($time_format));

    $this->assertEquals('2', current($this->xpath("//input[@id = 'edit-{$field_name}-0-duration-y']"))->getAttribute('value'), 'Duration years is set correctly.');
    $this->assertEquals('1', current($this->xpath("//input[@id = 'edit-{$field_name}-0-duration-m']"))->getAttribute('value'), 'Default duration months is set correctly.');
    $this->assertEquals('4', current($this->xpath("//input[@id = 'edit-{$field_name}-0-duration-d']"))->getAttribute('value'), 'Default duration days is set correctly.');
    $this->assertEquals('5', current($this->xpath("//input[@id = 'edit-{$field_name}-0-duration-h']"))->getAttribute('value'), 'Default duration hours is set correctly.');
    $this->assertEquals('10', current($this->xpath("//input[@id = 'edit-{$field_name}-0-duration-i']"))->getAttribute('value'), 'Default duration minutes is set correctly.');
    $this->assertEquals('45', current($this->xpath("//input[@id = 'edit-{$field_name}-0-duration-s']"))->getAttribute('value'), 'Default duration seconds is set correctly.');

    // start_date is '1917-11-07 03:05:00'.
    // end_value should be '1919-12-11 08:15:45'.
    $this->assertEquals('1919-12-11', current($this->xpath("//input[@id = 'edit-{$field_name}-0-end-value-date']"))->getAttribute('value'), 'End date is set correctly.');
    $this->assertEquals('08:15:45', current($this->xpath("//input[@id = 'edit-{$field_name}-0-end-value-time']"))->getAttribute('value'), 'End time is set correctly.');

    // Test the widget for validation notifications.
    // Change the widget settings to use the full duration granularity, but
    // no default duration.
    $this->getFormDisplay($this->field->getTargetEntityTypeId(), $this->field->getTargetBundle(), 'default')
      ->setComponent($field_name, [
        'type' => 'daterange_duration',
        'settings' => [
          'duration_granularity' => 'y:m:d:h:i:s',
          'time_increment' => '1',
          'default_duration' => [],
        ],
      ])
      ->save();
    $this->container->get('entity_field.manager')->clearCachedFieldDefinitions();

    // Define a start time, pick duration, set it to empty, end time should be
    // the same as start time.
    $this->drupalGet('entity_test/add');
    $edit = [
      "{$field_name}[0][value][date]" => '1917-11-07',
      "{$field_name}[0][value][time]" => '03:00:00',
      "{$field_name}[0][end_type]" => 'duration',
    ];
    $this->submitForm($edit, t('Save'));
    $this->assertSession()->statusCodeEquals(200);
    preg_match('|entity_test/manage/(\d+)|', $this->getUrl(), $match);
    $id = $match[1];
    $this->assertSession()->pageTextContains(t('entity_test @id has been created.', ['@id' => $id]));
    $this->assertEquals('1917-11-07', current($this->xpath("//input[@id = 'edit-{$field_name}-0-end-value-date']"))->getAttribute('value'), 'End date is set correctly.');
    $this->assertEquals('03:00:00', current($this->xpath("//input[@id = 'edit-{$field_name}-0-end-value-time']"))->getAttribute('value'), 'End time is set correctly.');
    $this->assertEquals('0', current($this->xpath("//input[@id = 'edit-{$field_name}-0-duration-y']"))->getAttribute('value'), 'Duration years is empty.');
    $this->assertEquals('0', current($this->xpath("//input[@id = 'edit-{$field_name}-0-duration-m']"))->getAttribute('value'), 'Default duration months is empty.');
    $this->assertEquals('0', current($this->xpath("//input[@id = 'edit-{$field_name}-0-duration-d']"))->getAttribute('value'), 'Default duration days is empty.');
    $this->assertEquals('0', current($this->xpath("//input[@id = 'edit-{$field_name}-0-duration-h']"))->getAttribute('value'), 'Default duration hours is empty.');
    $this->assertEquals('0', current($this->xpath("//input[@id = 'edit-{$field_name}-0-duration-i']"))->getAttribute('value'), 'Default duration minutes is empty.');
    $this->assertEquals('0', current($this->xpath("//input[@id = 'edit-{$field_name}-0-duration-s']"))->getAttribute('value'), 'Default duration seconds is empty.');

    // Define a start time, use end_date radio, but leave it empty.
    $this->drupalGet('entity_test/add');
    $edit = [
      "{$field_name}[0][value][date]" => '1917-11-07',
      "{$field_name}[0][value][time]" => '03:00:00',
      "{$field_name}[0][end_type]" => 'end_date',
    ];
    $this->submitForm($edit, t('Save'));
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains(t('You must define either a duration or an end date.'));

    // Now, set the field to not be required and try again.
    $this->field->setRequired(FALSE)->save();

    // We should hit core's validation error about a partial daterange value.
    $this->drupalGet('entity_test/add');
    // Intentionally re-using the same $edit array from above.
    $this->submitForm($edit, t('Save'));
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains(t('This value should not be null.'));

    // Completely empty values, field isn't required, should be no problem.
    $this->drupalGet('entity_test/add');
    $this->submitForm([], t('Save'));
    $this->assertSession()->statusCodeEquals(200);
    preg_match('|entity_test/manage/(\d+)|', $this->getUrl(), $match);
    $id = $match[1];
    $this->assertSession()->pageTextContains(t('entity_test @id has been created.', ['@id' => $id]));
  }

  /**
   * Tests Date Range Duration Widget functionality for date-only fields.
   */
  public function testDateRangeDateOnlyDurationWidget() {
    $field_name = $this->fieldStorage->getName();

    // Ensure field is set to a datetime field.
    $this->fieldStorage->setSetting('datetime_type', DateRangeItem::DATETIME_TYPE_DATE);
    $this->fieldStorage->save();

    // Change the widget to a daterange_duration with some default settings.
    $this->getFormDisplay($this->field->getTargetEntityTypeId(), $this->field->getTargetBundle(), 'default')
      ->setComponent($field_name, [
        'type' => 'daterange_duration',
        'settings' => [
          'duration_granularity' => 'm:d',
          'time_increment' => '86400',
          'default_duration' => [
            'd' => '2',
          ],
        ],
      ])
      ->save();
    $this->container->get('entity_field.manager')->clearCachedFieldDefinitions();

    // Display creation form.
    $this->drupalGet('entity_test/add');
    $this->assertSession()->fieldExists("{$field_name}[0][value][date]");
    $this->assertSession()->fieldNotExists("{$field_name}[0][value][time]");

    $end_type_id = "edit-{$field_name}-0-end-type";
    $duration_radio = $this->xpath('//div[@id=:id]//input[@value=:value]', [
      ':id' => $end_type_id,
      ':value' => 'duration',
    ]);
    $this->assertCount(1, $duration_radio, 'A radio button has a "Duration" choice.');
    $end_date_radio = $this->xpath('//div[@id=:id]//input[@value=:value]', [
      ':id' => $end_type_id,
      ':value' => 'end_date',
    ]);
    $this->assertCount(1, $end_date_radio, 'A radio button has an "End date" choice.');

    // No JS, this should still be visible.
    $this->assertSession()->fieldExists("{$field_name}[0][end_value][date]");
    // But this shouldn't exist, since it's a date-only field.
    $this->assertSession()->fieldNotExists("{$field_name}[0][end_value][time]");

    // First, submit a start date, using the default 2 day duration (and no end
    // date) and ensure the end_value is set correctly:
    $start_date = new DrupalDateTime('1917-11-05');
    $date_format = DateFormat::load('html_date')->getPattern();

    $edit = [
      "{$field_name}[0][value][date]" => $start_date->format($date_format),
      "{$field_name}[0][end_type]" => 'duration',
    ];
    $this->submitForm($edit, t('Save'));
    preg_match('|entity_test/manage/(\d+)|', $this->getUrl(), $match);
    $id = $match[1];
    $this->assertSession()->statusMessageContains(t('entity_test @id has been created.', ['@id' => $id]), 'status');

    // The end date is hidden by default, so we can't just assertRaw() for the
    // dates. Instead, assert the field values in the widget.
    $this->assertSession()->fieldExists("{$field_name}[0][value][date]");
    $this->assertSession()->fieldValueEquals("{$field_name}[0][value][date]", '1917-11-05');
    $this->assertSession()->fieldExists("{$field_name}[0][duration][d]");
    $this->assertSession()->fieldValueEquals("{$field_name}[0][duration][d]", '2');
    $this->assertSession()->fieldExists("{$field_name}[0][end_value][date]");
    $this->assertSession()->fieldValueEquals("{$field_name}[0][end_value][date]", '1917-11-06');

    // Now, submit a start date, change the duration, still no end date:
    $start_date = new DrupalDateTime('1917-11-07');
    $edit = [
      "{$field_name}[0][value][date]" => $start_date->format($date_format),
      "{$field_name}[0][end_type]" => 'duration',
      // 10 days that shook the world.
      "{$field_name}[0][duration][d]" => 10,
    ];
    $this->submitForm($edit, t('Save'));

    // Make sure the new start time is set.
    $this->assertSession()->fieldValueEquals("{$field_name}[0][value][date]", '1917-11-07');
    $this->assertSession()->fieldValueEquals("{$field_name}[0][duration][d]", '10');
    // Since this is date-only, 11-07 to 11-16 would be 10 total days.
    $this->assertSession()->fieldValueEquals("{$field_name}[0][end_value][date]", '1917-11-16');

    // Now, set a new end date directly, leaving the duration values in the
    // form, and see that when the page reloads, we have the right end time and
    // the correct new duration values pre-filled.
    $edit = [
      "{$field_name}[0][value][date]" => $start_date->format($date_format),
      "{$field_name}[0][end_type]" => 'end_date',
      "{$field_name}[0][end_value][date]" => '1917-11-20',
    ];
    $this->submitForm($edit, t('Save'));

    $this->assertSession()->fieldValueEquals("{$field_name}[0][value][date]", '1917-11-07');
    $this->assertSession()->fieldValueEquals("{$field_name}[0][end_value][date]", '1917-11-20');
    // start_date is '1917-11-07'.
    // end_value is '1917-11-20'.
    // Duration should be 14 total days.
    $this->assertSession()->fieldValueEquals("{$field_name}[0][duration][d]", '14');
  }

}
